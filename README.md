# html2csv

A coarse "HTML tables to CSV" (Comma-Separated Values) converter. All tables from the HTML file will be converted (as they occur) into a single CSV file, suited for loading into a spreadsheet editor.

* Can convert arbitrary size HTML files.
* Supports badly-formatted HTML (missing tag, etc.).

To convert a bunch of HTML files, just type: `python html2csv.py *.html`

This is also an example of the use of the [HTMLParser module](http://docs.python.org/2/library/htmlparser.html).

Released into the public domain by [Seb Sauvage](http://sebsauvage.net/) in 2002.

[html2csv on sebsauvage.net](http://sebsauvage.net/python/html2csv.py)

# Unit Tests

drone.io build status: [![Build Status](https://drone.io/bitbucket.org/isme/html2csv/status.png)](https://drone.io/bitbucket.org/isme/html2csv/latest)

In 2013 I added some unit tests because I had to modify the script's handling of <br> tags.

Seb said that the script supports badly-formatted HTML. One day I'll write tests to prove it!
