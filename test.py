import unittest
import html2csv

class Html2CsvTest(unittest.TestCase):
    
    def setUp(self):
      self.parser = html2csv.html2csv()
    
    def test_parse_None_raises_TypeError(self):
      with self.assertRaises(TypeError) as cm:
        self.parser.feed(None)
      
    def test_empty(self):
      self.parser.feed('')
      self.assertEqual(self.parser.getCSV(), '')
    
    def test_no_table(self):
      self.parser.feed(
          '<html>'
          '  <body>'
          '    <p>No tables here!</p>'
          '  </body>'
          '</html>'
      )
      self.assertEqual(self.parser.getCSV(), '')
    
    def test_empty_table(self):
      self.parser.feed('<table></table>')
      self.assertEqual(self.parser.getCSV(), '')
    
    def test_one_row_one_cell(self):
      self.parser.feed(
          '<table>'
          '  <tr><td>Unity</td></tr>'
          '</table>'
      )
      self.assertEqual(self.parser.getCSV(),
          '"Unity"\n'
      )
    
    def test_one_row_many_cells(self):
      self.parser.feed(
          '<table>'
          '  <tr>'
          '    <td>Buenos</td>'
          '    <td>Tardes</td>'
          '    <td>Tio</td>'
          '  </tr>'
          '</table>'
      )
      self.assertEqual(self.parser.getCSV(),
          '"Buenos","Tardes","Tio"\n'
      )
    
    def test_many_rows_one_cell(self):
      self.parser.feed(
          '<table>'
          '  <tr><td>Uno</td></tr>'
          '  <tr><td>Dos</td></tr>'
          '  <tr><td>Tres</td></tr>'
          '</table>'
      )
      self.assertEquals(self.parser.getCSV(),
          '"Uno"\n'
          '"Dos"\n'
          '"Tres"\n'
      )
    
    def test_many_rows_many_cells(self):
      self.parser.feed(
          '<table>'
          '  <tr><td>Norte</td><td>Est</td></tr>'
          '  <tr><td>Oest</td><td>Sud</td></tr>'
          '</table>'
      )
      self.assertEquals(self.parser.getCSV(),
          '"Norte","Est"\n'
          '"Oest","Sud"\n'
      )
    
    def test_comma_in_cell(self):
      self.parser.feed(
          '<table>'
          '  <tr>'
          '    <td>Hickory, Dickory, Dock</td>'
          '    <td>The mouse ran up the clock</td>'
          '  </tr>'
          '</table>'
      )
      self.assertEqual(self.parser.getCSV(),
          '"Hickory, Dickory, Dock","The mouse ran up the clock"\n'
      )
     
    def test_quotes_in_cell(self):
       self.parser.feed(
           '<table>'
           '  <tr><td>"To be or not to be" is hard to express in Chinese.</td></tr>'
           '</table>'
       )
       self.assertEquals(self.parser.getCSV(),
           '"""To be or not to be"" is hard to express in Chinese."\n'
       )
    
    def test_parse_br_in_cell_as_newline(self):
      self.parser.feed(
          '<table>'
          '  <tr><td>Funky<br>beats</td></tr>'
          '</table>'
      )
      self.assertEquals(self.parser.getCSV(),
          '"Funky\nbeats"\n',
          'Parse <br> tag in table cell as a newline character.'
      )
      
if __name__ == '__main__':
  unittest.main()